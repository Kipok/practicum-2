%include 'io.inc'

section .text
global CMAIN

CMAIN:
    GET_CHAR al;
    GET_CHAR bl;
    mov cl, 72; cl = 'H'
    sub al, cl
    neg al; al = 8 - a
    mov cl, 56
    sub cl, bl
    mul cl; al = (8 - a) * (8 - b)
    mov cl, 2
    div cl
    PRINT_DEC 1, al
    NEWLINE
    xor eax, eax
    ret
