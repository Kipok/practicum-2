%include 'io.inc'

section .bss
    a resd 1
    b resd 1
    c resd 1

section .text
global CMAIN

CMAIN:
    GET_HEX 4, a
    GET_HEX 4, b
    GET_HEX 4, c
    mov eax, dword[c]
    mov ebx, dword[c]
    not ebx
    and eax, dword[a]
    and ebx, dword[b]
    or eax, ebx
    PRINT_HEX 4, eax
    NEWLINE
    xor eax, eax
    ret
