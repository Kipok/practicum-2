%include 'io.inc'

section .bss
  a resd 1
  b resd 1
  c resd 1
  d resd 1
  e resd 1
  f resd 1

section .text
global CMAIN

CMAIN:
  GET_DEC 4, a
  GET_DEC 4, b
  GET_DEC 4, c
  GET_DEC 4, d
  GET_DEC 4, e
  GET_DEC 4, f
  mov eax, dword[e]; eax = e
  add eax, dword[f]; eax = e + F
  mul dword[a]; eax = a(e + f)
  mov ebx, eax
  mov eax, dword[d]; eax = d
  add eax, dword[f]; eax = d + f
  mul dword[b]; eax = b(d + f)
  add ebx, eax
  mov eax, dword[e]; eax = e
  add eax, dword[d]; eax = d + e
  mul dword[c]; eax = a(d + e)
  add ebx, eax
  PRINT_DEC 4, ebx
  NEWLINE
  xor eax, eax;
  ret
  
