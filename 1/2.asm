%include 'io.inc'

section .bss
  a resd 1
  b resd 1
  c resd 1
  v resd 1

section .text
global CMAIN

CMAIN:
  GET_DEC 4, a
  GET_DEC 4, b
  GET_DEC 4, c
  GET_DEC 4, v
  mov eax, dword[a]
  mul dword[b]
  div dword[v]; eax <- ab / v
  mov ecx, edx
  mul dword[c]; eax <- (ab / v) * c
  mov edx, ecx
  mov ecx, eax
  mov eax, edx
  mul dword[c]; eax <- (ab % v) * c
  div dword[v]
  add eax, ecx
  PRINT_DEC 4, eax
  NEWLINE
  xor eax, eax
  ret
