%include 'io.inc'

section .bss
    a11 resd 1
    a12 resd 1
    a21 resd 1
    a22 resd 1
    b1 resd 1
    b2 resd 1

section .text
global CMAIN

CMAIN:
    GET_UDEC 4, a11
    GET_UDEC 4, a12
    GET_UDEC 4, a21
    GET_UDEC 4, a22
    GET_UDEC 4, b1
    GET_UDEC 4, b2
    ; x = 1, y = 1
    mov eax, dword[a11]; eax = x = a11
    xor eax, dword[a12]; eax = x = a11 ^ a12
    xor eax, dword[b1]; eax = x = a11 ^ a12 ^ b1
    not eax; eax = x = a11 ^ a12 ^ b1 ^ 1
    mov ecx, dword[a21]; ecx = a21
    xor ecx, dword[a22]; ecx = a21 ^ a22
    xor ecx, dword[b2]; ecx = a21 ^ a22 ^ b2
    not ecx; ecx = a21 ^ a22 ^ b2 ^ 1
    and eax, ecx; eax = x = (a11 ^ a12 ^ b1 ^ 1) & (a21 ^ a22 ^ b2 ^ 1)
    mov ebx, eax; ebx = y = (a11 ^ a12 ^ b1 ^ 1) & (a21 ^ a22 ^ b2 ^ 1)
    ; x = 1, y = 0
    mov ecx, dword[a11]; ecx = a11
    xor ecx, dword[b1]; ecx = a11 ^ b1
    not ecx; ecx = a11 ^ b1 ^ 1
    mov edx, dword[a21]; edx = a21
    xor edx, dword[b2]; edx = a21 ^ b2
    not edx; edx = a21 ^ b2 ^ 1
    and ecx, edx; ecx = (a11 ^ b1 ^ 1) & (a21 ^ b2 ^ 1)
    mov esi, eax
    not esi; esi = !x
    and ecx, esi
    mov esi, ebx
    not esi; esi = !y
    and ecx, esi; ecx = (a11 ^ b1 ^ 1) & (a21 ^ b2 ^ 1) & !x & !y
    xor eax, ecx
    ; x = 0, y = 1
    mov ecx, dword[a12]
    xor ecx, dword[b1]
    not ecx
    mov edx, dword[a22]
    xor edx, dword[b2]
    not edx
    and ecx, edx
    mov esi, eax
    not esi; esi = !x
    and ecx, esi
    mov esi, ebx
    not esi; esi = !y
    and ecx, esi; ecx = (a12 ^ b1 ^ 1) & (a22 ^ b2 ^ 1) & !x & !y
    xor ebx, ecx

    PRINT_UDEC 4, eax
    NEWLINE
    PRINT_UDEC 4, ebx
    NEWLINE
    xor eax, eax
    ret


