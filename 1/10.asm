%include 'io.inc'

section .bss
    m resd 1
    d resd 1

section .text
global CMAIN

CMAIN:
    GET_DEC 4, m
    GET_DEC 4, d
    mov eax, dword[m]
    dec eax; --m
    mov ecx, 2
    cdq
    div ecx; eax = m / 2
    mov ecx, 83
    mov ebx, edx
    mul ecx; eax = 83 * m / 2
    mov edx, ebx
    mov ebx, eax
    mov eax, edx
    mov ecx, 41
    mul ecx; eax = 41 * m % 2
    add ebx, eax
    add ebx, dword[d]
    PRINT_DEC 4, ebx
    NEWLINE
    xor eax, eax
    ret
