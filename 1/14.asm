%include 'io.inc'

section .bss
    n resd 1
    m resd 1
    k resd 1
    d resd 1
    x resd 1
    y resd 1

section .text
global CMAIN

CMAIN:
    GET_DEC 4, n
    GET_DEC 4, m
    GET_DEC 4, k
    GET_DEC 4, d
    GET_DEC 4, x
    GET_DEC 4, y
    mov eax, dword[n]
    mul dword[m]
    mul dword[k]
    add eax, dword[d]
    dec eax
    div dword[d]; eax = ceil(n * m * k / d)
    mov ecx, eax
    mov eax, 60
    mul dword[x]
    add eax, dword[y]; eax = 60x + y
    xor ebx, ebx
    sub eax, 360
    setae bl; ebx shows if we have to subtract anything
    mov eax, ecx
    mov esi, 3
    cdq
    div esi; eax = ceil(nmk/d)/3
    test edx, edx
    setnz dl
    add eax, edx
    mul ebx
    sub ecx, eax
    PRINT_DEC 4, ecx
    NEWLINE
    xor eax, eax
    ret
