%include 'io.inc'

section .bss
    n resd 1
    k resd 1
    p resd 1
    ans resd 1

section .test
    global CMAIN
CMAIN:
    GET_DEC 4, n
    GET_DEC 4, k
    xor ecx, ecx                    ; i = 0
    mov ebx, 1
.find_first:
    mov eax, dword[n]
    and eax, ebx                    ; eax = n & (1 << i)
    test eax, eax
    jz .fend
    mov dword[p], ecx
.fend:    
    shl ebx, 1 
    inc ecx
    cmp ecx, 30                     ; i < 30
    jnz .find_first                 ; this loop is to find first '1' in n

    mov ecx, dword[p]               ; i = p
    mov ebx, 1
    shl ebx, cl                     ; ebx = (1 << p)
.sum:
    mov eax, dword[n]
    and eax, ebx                    ; eax = n & (1 << i)
    test eax, eax
    jz .send
    cmp ecx, dword[p]
    jz .stcount
    mov eax, dword[p]
    sub eax, ecx
    dec eax                         ; eax = p - i - 1
    sub dword[k], eax               ; k -= p - i - 1
    mov dword[p], ecx               ; p = i
    mov edi, dword[k]
    dec edi
    push edi
    push ecx
    call C
    add esp, 8
    add dword[ans], eax             ; ans += C(i, k - 1)
    jmp .send
.stcount:
    mov esi, ecx
    dec esi                         ; j = i - 1
    mov edi, dword[k]               ; edi = k
    cmp esi, edi
    jl .send
    .countC:
        push edi
        push esi
        call C                      
        add esp, 8
        add dword[ans], eax         ; ans += C(j, k)
        dec esi
        cmp esi, edi
        jge .countC                 ; j >= k
.send:
    
    shr ebx, 1
    dec ecx
    cmp ecx, 0
    jge .sum                        ; i >= 0
    
    mov ebx, dword[k]
    cmp dword[p], ebx              
    jne .print
    inc dword[ans]                  ; ans += (p == k)
.print:
    PRINT_DEC 4, [ans]
    NEWLINE
    xor eax, eax
    ret

C:
    push ebp
    mov ebp, esp
    push ecx
    push ebx
    push esi
    add ebp, 12                     ; [ebp] - k, [ebp - 4] - n
    mov ebx, dword[ebp]             ; ebx = k
    cmp ebx, 0                      ; if (k < 0)
    jge .next2
    mov eax, 0
    jmp .end                        ; return 0;
.next2:
    sub ebx, dword[ebp - 4]
    cmp ebx, 0                      ; if (n < k)
    jle .next3
    xor eax, eax
    jmp .end                        ; return 0;
.next3:
    neg ebx
    inc ebx
    mov eax, 1                      ; ans = 1
    mov ecx, ebx                    ; i = n - k + 21
    mov esi, 1                      ; j = 1
    cmp ecx, dword[ebp - 4]
    jg .end
.cfor:
    mul ecx
    div esi                        ; ans = long long(ans * i) / j
    inc ecx
    inc esi
    cmp ecx, dword[ebp - 4]
    jle .cfor
.end:
    pop esi
    pop ebx
    pop ecx
    pop ebp
    ret
