#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

int n, k, a[21], b[21];

int 
main(void)
{
    scanf("%d%d", &n, &k);
    a[0] = 1;
    int i = 0, *c = a, *d = b;
    while (i != n) {
        ++i;
        d[0] = 1;
        int j = 0;
        while (j != i) {
            ++j;
            d[j] = c[j] + c[j - 1];
        }
        d[i] = 1;
        c = (c == b) ? a : b;
        d = (d == a) ? b : a;
    }
    printf("%d\n", c[k]);
    return 0;
}