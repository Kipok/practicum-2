#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

int 
main(void)
{
    unsigned long long x;
    scanf("%llu", &x);
    if (!x)
        x = 1 << 31;
    printf("%u\n", (unsigned int)(((x ^ (x - 1)) + 1) >> 1));
    return 0;
}