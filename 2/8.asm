%include "io.inc"

section .bss
    MAX_SIZE equ 10002
    n resd 1
    m resd 1
    k resd 1
    a resd MAX_SIZE
    b resd MAX_SIZE
    c resd MAX_SIZE

section .text
    global CMAIN
CMAIN:
    GET_DEC 4, n
    GET_DEC 4, m
    GET_DEC 4, k
    mov ecx, dword[n]
    imul ecx, dword[m]              ; ecx = n * m
    xor edx, edx
.readA:
    GET_DEC 4, [a + edx * 4]
    inc edx
    cmp edx, ecx
    jne .readA                      ; read matrix A

    mov ecx, dword[m]
    imul ecx, dword[k]              ; ecx = m * k
    xor edx, edx
.readB:
    GET_DEC 4, [b + edx * 4]
    inc edx
    cmp edx, ecx
    jne .readB                                  ; read matrix B

    xor ecx, ecx                                ; ecx = i
.forN:
    xor esi, esi                                ; esi = j
    .forK:
        xor edi, edi                            ; edi = t
        .forM:
            mov ebx, ecx
            imul ebx, dword[m]                  ; ebx = ecx * m
            add ebx, edi
            mov eax, dword[a + ebx * 4]             ; eax = a[i][t]
            mov ebx, edi
            imul ebx, dword[k]                  ; ebx = edi * k
            add ebx, esi
            mov edx, dword[b + ebx * 4]             ; edx = b[t][j]
            imul edx
            mov ebx, ecx
            imul ebx, dword[k]                  ; ebx = ecx * k
            add ebx, esi
            add dword[c + ebx * 4], eax             ; c[i][j] += a[i][t] * b[t][j]
            inc edi
            cmp edi, dword[m]
            jne .forM
        mov ebx, ecx
        imul ebx, dword[k]                      ; ebx = ecx * k
        add ebx, esi
        PRINT_DEC 4, [c + ebx * 4]             ; print c[i][j]
        PRINT_CHAR ' '
        inc esi
        cmp esi, dword[k]
        jne .forK
    NEWLINE
    inc ecx
    cmp ecx, dword[n]
    jne .forN
    
    xor eax, eax
    ret


