%include "io.inc"

section .text
    global CMAIN
CMAIN:
    GET_DEC 4, esi      ; esi = a
    GET_DEC 4, edi      ; edi = b
.while:
    mov ecx, esi        ; r = a
    mov esi, edi        ; a = b
    mov eax, ecx
    cdq
    div edi
    mov edi, edx        ; b = r % b
    test edi, edi
    jnz .while
    PRINT_DEC 4, esi
    NEWLINE
    xor eax, eax
    ret
