%include "io.inc"

section .bss
    n resd 1
    MIN_INT equ 0x80000000

section .text
    global CMAIN
CMAIN:
    GET_DEC 4, n
    mov eax, MIN_INT        ; eax - max
    mov ebx, MIN_INT        ; ebx - second max
    mov edx, MIN_INT        ; edx - third max
    xor ecx, ecx            ; ecx - loop counter
.for:
    GET_DEC 4, esi          ; esi - current element
    cmp esi, eax
    jl .ls1
    mov edx, ebx            ; a[i] >= max
    mov ebx, eax
    mov eax, esi
    jmp .end
.ls1:
    cmp esi, ebx
    jl .ls2
    mov edx, ebx            ; a[i] >= 2nd_max
    mov ebx, esi
    jmp .end
.ls2:
    cmp esi, edx
    jl .end
    mov edx, esi            ; a[i] >= 3rd_max
.end:
    inc ecx
    cmp ecx, dword[n]
    jne .for

    PRINT_DEC 4, eax
    NEWLINE
    PRINT_DEC 4, ebx
    NEWLINE
    PRINT_DEC 4, edx
    NEWLINE
    xor eax, eax
    ret
