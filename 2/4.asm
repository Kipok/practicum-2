%include "io.inc"

section .bss
    n resd 1
    a resb 15

section .text
    global CMAIN
CMAIN:
    GET_UDEC 4, n
    mov edi, 13
    mov ecx, dword[n]
    test ecx, ecx
    jnz .while
    PRINT_DEC 4, ecx
    jmp .end
.while:
    xor ebx, ebx                ; ebx = 0
    mov edx, ecx
    and edx, 1                  ; edx = last bit of number
    add ebx, edx                
    shr ecx, 1
    mov edx, ecx
    and edx, 1
    lea ebx, [ebx + edx * 2]
    shr ecx, 1
    mov edx, ecx
    and edx, 1
    lea ebx, [ebx + edx * 4]    ; ebx = last three bits of number in decimal
    shr ecx, 1
    mov byte[a + edi], bl
    dec edi
    test edi, edi
    jnz .while

   mov edi, 1 
.delzero:
    inc edi
    mov al, byte[a + edi]
    test al, al
    jz .delzero

.print:
    mov al, byte[a + edi]
    PRINT_DEC 1, al
    inc edi
    cmp edi, 14
    jnz .print
.end:
    NEWLINE
    xor eax, eax
    ret
