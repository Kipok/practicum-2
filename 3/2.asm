%include 'io.inc'

section .text
    global CMAIN
CMAIN:
	GET_UDEC 4, esi
    sub esp, 4
    mov [esp], esi
    call count
    add esp, 4
    PRINT_DEC 4, eax
    NEWLINE
    xor eax, eax
    ret

count:
    push ebp
    mov ebp, esp                ; [ebp + 8] -- a
    push ebx
    mov ebx, [ebp + 8]          ; ebx = a
    cmp ebx, 3
    jae .goto_rec               ; if (a < 3)
    xor eax, eax                
    cmp ebx, 1
    jnz .end
    inc eax
    jmp .end                    ; return a == 1
.goto_rec:
    mov eax, ebx
    mov ebx, 3
    xor edx, edx                ; eax = a / 3
    div ebx                     ; edx = a % 3
    mov ebx, edx                ; ebx = a % 3
    sub esp, 4
    mov [esp], eax
    call count
    add esp, 4                  ; eax = count (a / 3)
    cmp ebx, 1
    jnz .end
    inc eax
.end:
    pop ebx
    pop ebp
    ret
    


