#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    int a, b, c;
    scanf("%d%d%d", &a, &b, &c);
    if ((a <= b && a >= c) || (a <= c && a >= b)) {
        printf("%d", a);
        return 0;
    }
    if ((b <= a && b >= c) || (b <= c && b >= a)) {
        printf("%d", b);
        return 0;
    }
    if ((c <= a && c >= b) || (c <= b && c >= a)) {
        printf("%d", c);
    }
    return 0;
}
