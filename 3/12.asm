%include 'io.inc'

section .bss
    x1 resd 1
    x2 resd 1
    x3 resd 1
    y1 resd 1
    y2 resd 1
    y3 resd 1

section .text
    global CMAIN
CMAIN:
    push ebp
    mov ebp, esp
    GET_DEC 4, x1
    GET_DEC 4, y1
    GET_DEC 4, x2
    GET_DEC 4, y2
    GET_DEC 4, x3
    GET_DEC 4, y3
    mov ecx, 0
    mov eax, dword[y2]
    sub eax, dword[y1]
    cmp eax, 0
    jge .abs1
    neg eax
.abs1:
    push eax 
              
    mov eax, dword[x2]
    sub eax, dword[x1]
    cmp eax, 0
    jge .abs2
    neg eax
.abs2:
    push eax  
    call gcd
    add esp, 8
    add ecx, eax 
    
    mov eax, dword[y3]
    sub eax, dword[y1]
    cmp eax, 0
    jge .abs3
    neg eax
.abs3:
    push eax 
          
    mov eax, dword[x3]
    sub eax, dword[x1]
    cmp eax, 0
    jge .abs5
    neg eax
.abs5:
    push eax
    call gcd   
    add esp, 8
    add ecx, eax 
    
    mov eax, dword[x3]
    sub eax, dword[x2]
    cmp eax, 0
    jge .abs6
    neg eax
.abs6:
    push eax 
          
    mov eax, dword[y3]
    sub eax, dword[y2]
    cmp eax, 0
    jge .abs7
    neg eax
.abs7:
    push eax
    call gcd  
    add esp, 8
    add ecx, eax 
    
    push dword[y3]
    push dword[x3]
    push dword[y2]
    push dword[x2]
    push dword[y1]
    push dword[x1]
    call S
    add esp, 24
    sub eax, ecx
    add eax, 2
    mov ebx, 2
    cdq
    div ebx
    PRINT_DEC 4, eax
    NEWLINE
    xor eax, eax
    mov esp, ebp
    pop ebp
    ret

S:
    push ebp
    mov ebp, esp
    push ebx   ; 8 - x1, 12 - y1, 16 - x2, 20 - y2, 24 - x3, 28 - y3
    push ecx
    push edx
    mov eax, dword[ebp + 16]       ; eax = x2
    sub eax, dword[ebp + 8]         ; eax = x2 - x1
    mov ebx, dword[ebp + 28]                   
    sub ebx, dword[ebp + 12]        ; ebx = y3 - y1
    mul ebx                         ; eax = (x2 - x1)*(y3 - y1)
    mov ecx, eax
    mov eax, dword[ebp + 24]
    sub eax, dword[ebp + 8]         ; eax = (x3 - x1)
    mov ebx, dword[ebp + 20]
    sub ebx, dword[ebp + 12]        ; ebx = (y2 - y1)
    mul ebx
    sub ecx, eax                    ; ecx = 2s
    mov eax, ecx
    cmp eax, 0
    jge .end
    neg eax
.end:
    pop edx
    pop ecx
    pop ebx 
    pop ebp
    ret
    
gcd:
    push ebp
    mov ebp, esp
    push esi
    push edi
    push ebx
    push edx
    push ecx
    mov esi, dword[ebp + 8]          ; esi = x
    mov edi, dword[ebp + 12]         ; edi = y
    test esi, esi
    jnz .startwhile
    mov eax, edi
    jmp .end_of_gcd
.startwhile:
    test edi, edi
    jnz .while
    mov eax, esi
    jmp .end_of_gcd
.while:
    mov ecx, esi        ; r = a
    mov esi, edi        ; a = b
    mov eax, ecx
    cdq
    div edi
    mov edi, edx        ; b = r % b
    test edi, edi
    jnz .while
    mov eax, dword[ebp + 8]
    cdq
    div esi                         ; eax = t = x / a
    mov ebx, eax
    mov eax, dword[ebp + 8]
    cdq
    div ebx                         ; return x / t
.end_of_gcd:
    pop ecx
    pop edx
    pop ebx
    pop edi
    pop esi
    pop ebp
    ret
    