%include 'io.inc'

section .text
    global CMAIN
CMAIN:
	GET_UDEC 4, esi
    GET_UDEC 4, edi
    mov ebx, esi
.while:
    sub esp, 8
    mov [esp], esi
    mov [esp + 4], edi
    call count
    add esp, 8
    add ebx, eax
    cmp esi, eax
    jz .end_of_program
    mov esi, eax
    jmp .while
.end_of_program:
    PRINT_UDEC 4, ebx
    NEWLINE
    xor eax, eax
    ret

count:
    push ebp
    mov ebp, esp                ; [ebp + 8] -- a
                                ; [ebp + 12] -- k
    push ebx
    push ecx
    mov ebx, [ebp + 8]          ; ebx = a
    mov ecx, [ebp + 12]         ; ecx = k
    cmp ebx, ecx
    jae .goto_rec               ; if (a < k)
    mov eax, ebx
    jmp .end                    ; return a
.goto_rec:
    mov eax, ebx
    mov ebx, ecx
    xor edx, edx                ; eax = a / k
    div ebx                     ; edx = a % k
    mov ebx, edx                ; ebx = a % k
    sub esp, 8
    mov [esp], eax
    mov [esp + 4], ecx
    call count
    add esp, 8                  ; eax = count (a / k)
    add eax, ebx
.end:
    pop ecx
    pop ebx
    pop ebp
    ret
    


