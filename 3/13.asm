%include 'io.inc'

section .bss
    u resd 11

section .text
    global CMAIN
CMAIN:
    GET_DEC 4, ebx
    GET_DEC 4, esi
    GET_DEC 4, edi
    push edi
    push esi
    push ebx
    call get_ans
    add esp, 12
    NEWLINE
    xor eax, eax
    ret

cvar:
    push ebp
    mov ebp, esp
    push ecx
    push ebx
    add ebp, 12                     ; [ebp] - k, [ebp - 4] - n
    mov ebx, dword[ebp]             ; ebx = k
    sub ebx, dword[ebp - 4]
    cmp ebx, 0                      ; if (n < k)
    jle .next
    xor eax, eax
    jmp .end                        ; return 0;
.next:
    neg ebx
    inc ebx
    mov eax, 1                      ; ans = 1
    mov ecx, ebx                    ; i = n - k + 21
    cmp ecx, dword[ebp - 4]
    jg .end
.cfor:
    mul ecx
    inc ecx
    cmp ecx, dword[ebp - 4]
    jle .cfor
.end:
    pop ebx
    pop ecx
    pop ebp
    ret

get_num:
    push ebp
    mov ebp, esp
    push ecx
    push edx
    xor eax, eax
    mov ecx, dword[ebp + 8]         ; ecx = x
.whilex:
    inc eax                         ; i++
    mov edx, dword[u + eax * 4]
    test edx, edx                   ; if (!u[i])
    jnz .endx
    dec ecx                         ; --x
.endx:
    test ecx, ecx
    jnz .whilex
    pop edx
    pop ecx
    pop ebp
    ret

get_ans:
    push ebp
    mov ebp, esp
    push ecx
    push ebx
    push esi
    push edi
    push edx
    cmp dword[ebp + 12], 1                ; [ebp + 8] = n, [ebp + 12] = k, [ebp + 16] = m
    jnz .ansrec
    push dword[ebp + 16]
    call get_num
    add esp, 4
    PRINT_DEC 4, eax
    jmp .ansend
.ansrec:
    mov ecx, dword[ebp + 12]
    dec ecx
    push ecx
    mov ecx, dword[ebp + 8]
    dec ecx
    push ecx
    call cvar
    add esp, 8
    mov ecx, eax                            ; ecx = c = cvar(n - 1, k - 1)
    mov eax, dword[ebp + 16]
    cdq
    div ecx                                 ; eax = m / c, edx = m % c
    test edx, edx
    jz .addmod
    inc eax                                 ; eax = m / c + !!(m % c)
.addmod:
    mov ebx, eax                            ; ebx = num
    push ebx
    call get_num
    add esp, 4                              ; eax = gnum
    PRINT_DEC 4, eax
    PRINT_CHAR ' '
    mov dword[u + eax * 4], 1
    mov eax, ebx
    dec eax
    mul ecx
    sub eax, dword[ebp + 16]
    neg eax                                 ; eax = m - (num - 1) * c
    push eax
    mov eax, dword[ebp + 12]
    dec eax
    push eax
    mov eax, dword[ebp + 8]
    dec eax
    push eax
    call get_ans
    add esp, 12
.ansend:
    pop edx
    pop edi
    pop esi
    pop ebx
    pop ecx
    pop ebp
    ret


