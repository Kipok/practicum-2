#include <stdio.h>
#include <stdlib.h>
#define BUF_SIZE 4095        

char s[BUF_SIZE + 1];
unsigned int x[256];

int hash(char *s)
{
    int k;
    for (int i = 0; i < 256; i++) {
        x[i] = i;
        for (int j = 0; j < 8; j++) {
            if (x[i] & 1)
                x[i] = (x[i] >> 1) ^ 0xEDB88320;
            else 
				x[i] >>= 1;
		}
    }
    int i = 0;
	unsigned int a = -1;
    while (s[i])
    {
		k = (s[i++] ^ a) & 255;
        a = (a >> 8) ^ x[k];
    }
    return (~a);
}

int main(void)
{
	fgets(s, BUF_SIZE, stdin);
	s[BUF_SIZE] = '\0';
	printf("%x\n", hash(s));
    return 0;
}