%include 'io.inc'

section .rodata
    sc_str db '%d', 0
    pr_str db '%d', 10, 0

CEXTERN free
CEXTERN malloc
CEXTERN realloc

section .text
    global CMAIN
CMAIN:
   	push   ebp
   	mov    ebp,esp
    and    esp, -16
   	sub    esp, 32
    mov    dword[ebp - 24], 1           ; sz
   	mov    dword[esp], 4
   	call   malloc
   	mov    dword[ebp - 8], eax          ; *a
   	mov    eax, dword [ebp - 8]
   	mov    dword[esp + 4], eax
   	mov    dword[esp], sc_str
   	call   scanf
   	mov    dword[ebp - 20], 0           ; i
   	jmp    .end_of_main_for
.main_for:
    add    dword[ebp - 20], 1			; if (i == sz)
   	mov    eax, dword[ebp - 24]
   	cmp    eax, dword[ebp - 20]
   	jne    .more_memory
   	shl    dword[ebp - 24], 1
   	mov    eax, dword[ebp - 24]
   	lea    edx, [eax * 4]
   	mov    eax, dword[ebp - 8]
   	mov    dword[esp + 4], edx
   	mov    dword[esp], eax
   	call   realloc
   	mov    dword[ebp - 8], eax
.more_memory:	
    mov    eax, dword[ebp - 20]
   	lea    edx, [eax * 4]
   	mov    eax, dword[ebp - 8]
   	add    eax, edx
   	mov    dword[esp + 4], eax
   	mov    dword[esp], sc_str
   	call   scanf
.end_of_main_for:
    mov    eax, dword[ebp - 20]
   	lea    edx, [eax * 4]
   	mov    eax, dword[ebp - 8]
   	add    eax, edx
   	mov    eax, dword[eax]
   	test   eax, eax
   	jne    .main_for						; while (a[i] != )
   	dec    dword[ebp - 20]					; --i
   	mov    eax, dword[ebp - 20]
   	lea    edx, [eax * 4]
   	mov    eax, dword[ebp - 8]
   	add    eax, edx
   	mov    eax, dword[eax]					; eax -- a[i]
   	mov    dword[ebp - 12], eax				; [ebp - 12] -- last element
   	mov    dword[ebp - 16], 0				; [ebp - 16] -- ans
   	dec    dword[ebp - 20]					; --i
   	jmp    .end_of_count_ans
.count_ans:
    mov    eax, dword[ebp - 20]
   	lea    edx, [eax * 4]
   	mov    eax, dword[ebp - 8]
   	add    eax, edx
   	mov    eax, dword[eax]					; eax -- a[i]
   	cmp    eax, dword[ebp - 12]
   	jge    .inc_ans
   	inc    dword[ebp - 16]
.inc_ans:
    dec    dword[ebp - 20]
.end_of_count_ans:
    cmp    dword[ebp - 20], 0
   	jge    .count_ans
   	mov    eax, dword[ebp - 16]
   	mov    dword[esp + 4], eax
   	mov    dword[esp], pr_str
   	call   printf
   	mov    eax, dword[ebp - 8]
   	mov    dword[esp], eax
   	call   free
   	xor    eax, eax
   	leave  
   	ret    
