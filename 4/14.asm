%include 'io.inc'

CEXTERN fopen
CEXTERN fclose
CEXTERN fscanf
CEXTERN fprintf
CEXTERN malloc
CEXTERN free

section .rodata
	in_str db 'input.txt',  0
	in_fmt db 'rt',  0
	out_str db 'output.txt',  0
	out_fmt db 'wt',  0
	sc_str db '%d',  0
	pr_str db '%d ',  0

section .text
	global CMAIN
CMAIN:
   	push   ebp
   	mov    ebp, esp
   	sub    esp, 40
   	and    esp, -16
   	mov    dword[esp + 4], in_fmt
   	mov    dword[esp], in_str
   	call   fopen										; [ebp - 4] -- FILE *fr
   	mov    dword[ebp - 4], eax							; FILE *fr = fopen("input.txt",  "rt")
   	mov    dword[esp + 4], out_fmt
   	mov    dword[esp], out_str
   	call   fopen										; [ebp - 8] -- FILE *fw
   	mov    dword[ebp - 8], eax							; FILE *fw = fopen("output.txt",  "wt")
   	mov    dword[ebp - 12], 0							; [ebp - 12] -- list *l
   	mov    dword[ebp - 16], 0							; [ebp - 16] -- res
.read_all:
	lea    eax, [ebp - 20]
   	mov    DWORD[esp + 8], eax
   	mov    dword[esp + 4], sc_str
   	mov    eax, dword[ebp - 4]
   	mov    dword[esp], eax
   	call   fscanf
   	cmp    eax, 1
   	jne    .end_of_while
   	mov    eax, dword[ebp - 20]
   	mov    dword[esp + 4], eax
   	mov    eax, dword[ebp - 12]
   	mov    dword[esp], eax
   	call   add_to_list
   	mov    dword[ebp - 12], eax							; l = add_to_list(l, a)
	jmp .read_all
.L1:	
   	jmp    .read_all
.end_of_while:	
	mov    eax, dword[ebp - 8]
   	mov    dword[esp + 4], eax
   	mov    eax, dword[ebp - 12]
   	mov    dword[esp], eax
   	call   print_list									; print_list(l, fw)
   	mov    eax, dword[ebp - 12]
   	mov    dword[esp], eax
   	call   delete_list									; delete_list(l)
   	mov    eax, dword[ebp - 4]
   	mov    dword[esp], eax
   	call   fclose										; fclose(fr)
   	mov    eax, dword[ebp - 8]
   	mov    dword[esp], eax	
   	call   fclose										; fclose(fw)
   	xor    eax, eax
   	leave  
   	ret    

add_to_list:	
	push   ebp
   	mov    ebp, esp
   	push   ebx
   	sub    esp, 20
   	cmp    dword[ebp + 8], 0
   	jne    .not_null	
   	mov    dword[esp], 8
   	call   malloc
   	mov    dword[ebp + 8], eax								; l = (list *)malloc(8)
   	mov    eax, dword[ebp + 8]									
   	mov    dword[eax], 0									; l->next = 0
   	mov    edx, dword[ebp + 8]
   	mov    eax, dword[ebp + 12]
   	mov    dword[edx + 4], eax								; l->value = x
   	jmp    .end_of_add_to_list
.not_null:	
	mov    eax, dword[ebp + 8]
   	mov    eax, dword[eax + 4]
   	cmp    eax, dword[ebp + 12]
   	jge    .stop_rec											; if (l->value < x)
   	mov    ebx, dword[ebp + 8]
   	mov    eax, dword[ebp + 12]
   	mov    dword[esp + 4], eax
   	mov    eax, dword[ebp + 8]
   	mov    eax, dword[eax]
   	mov    dword[esp], eax
   	call   add_to_list
   	mov    dword[ebx], eax									; l->next = add_to_list(l->next, x)
   	jmp    .end_of_add_to_list
.stop_rec:	
	mov    eax, dword[ebp + 8]
   	mov    dword[ebp - 8], eax
   	mov    dword[esp], 8
   	call   malloc
   	mov    dword[ebp + 8], eax
   	mov    edx, dword[ebp + 8]
   	mov    eax, dword[ebp - 8]
   	mov    dword[edx], eax
   	mov    edx, dword[ebp + 8]
   	mov    eax, dword[ebp + 12]
   	mov    dword[edx + 4], eax
.end_of_add_to_list:	
	mov    eax, dword[ebp + 8]
   	add    esp, 20
   	pop    ebx
   	pop    ebp
   	ret    

print_list:											; void print_list(list *l, FILE *fw)
	push   ebp
   	mov    ebp, esp
   	sub    esp, 24  
.print_while:	
	cmp    dword[ebp + 8], 0						; while (l)
   	je     .end_of_print_list
   	mov    eax, dword[ebp + 8]
   	mov    eax, dword[eax + 4]
   	mov    dword[esp + 8], eax
   	mov    dword[esp + 4], pr_str
   	mov    eax, dword[ebp + 12]
   	mov    dword[esp], eax
   	call   fprintf
   	mov    eax, dword[ebp + 8]
   	mov    eax, dword[eax]
   	mov    dword[ebp + 8], eax
   	jmp    .print_while
.end_of_print_list:	
	leave  
   	ret    
	
delete_list:										; void delete_list(list *l) 
	push   ebp
   	mov    ebp, esp
   	sub    esp, 8
   	cmp    dword[ebp + 8], 0
   	jne    .not_null_free
   	jmp    .end_of_delete_list
.not_null_free:	
	mov    eax, dword[ebp + 8]
   	mov    eax, dword[eax]
   	mov    dword[esp], eax
   	call   delete_list
   	mov    eax, dword[ebp + 8]
   	mov    dword[esp], eax
   	call   free
.end_of_delete_list:	
	leave  
   	ret    
