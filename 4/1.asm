%include "io.inc"

section .rodata
    sprint db '0x%08X', 10, 0
    sread db '%u', 0
section .bss
    x resd 1
    
section .text
global CMAIN
CMAIN:
    push ebp
    mov ebp, esp
    and esp, -16
    sub esp, 12
    mov edi, eax
.while:
    mov dword[esp], sread
    mov dword[esp + 4], x
    call scanf
    mov esi, eax                     ;scanf("%u", &x)
    cmp esi, 1
    jnz .blablabla
    mov dword[esp], sprint
    mov ebx, dword[x]
    mov dword[esp + 4], ebx
    call printf
                                     ; printf("0x%08X\n", x)
    cmp esi, 1
    jz .while                        ; while (scanf == 1)
.blablabla:
    mov esp, ebp
    pop ebp
    xor eax, eax
    ret
