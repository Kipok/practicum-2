%include 'io.inc'

CEXTERN fopen
CEXTERN fclose
CEXTERN fscanf
CEXTERN fprintf
CEXTERN free
CEXTERN malloc
CEXTERN calloc

section .rodata:
	in_str db 'input.txt', 0
	in_fmt db 'rt', 0
	out_str db 'output.txt', 0
	out_fmt db 'wt', 0
	sc_str db '%d%d', 0
	pr_str db '%d ', 0

section .text:
	global CMAIN
CMAIN:
   	push   ebp
   	mov    ebp, esp
   	push   ebx
   	sub    esp, 68
   	and    esp, -16

   	mov    dword[esp + 4], in_fmt
   	mov    dword[esp], in_str
   	call   fopen
   	mov    dword[ebp - 16], eax					; FILE *fr = fopen("input.txt", "rt")
   	lea    eax, [ebp - 28]						; [ebp - 32] -- int i
   	mov    dword[esp + 12], eax					; [ebp - 28] -- int m
   	lea    eax, [ebp - 24]						; [ebp - 24] -- int n
   	mov    dword[esp + 8], eax					; [ebp - 20] -- FILE *fp 
   	mov    dword[esp + 4], sc_str				; [ebp - 16] -- FILE *fr
   	mov    eax, dword[ebp - 16]					; [ebp - 12] -- node **pr
   	mov    dword[esp], eax						; [ebp - 8] -- list *l
   	call   fscanf								; fscanf(fr, "%d%d", &n, &m)

   	mov    dword[esp + 4], 4
   	mov    eax, dword[ebp - 24]
   	mov    dword[esp], eax
   	call   calloc
   	mov    dword[ebp - 12], eax					; node **pr = (node **)calloc(n, sizeof(node *))
   	mov    dword[esp], 12						; we have 3 elements each takes 4 bytes => 12 is the size of list
   	call   malloc
   	mov    dword[ebp - 8], eax					; list *l = (list *)malloc(sizeof(list))
   	mov    ebx, dword[ebp - 8]
   	mov    eax, dword[ebp - 12]
   	mov    dword[esp + 16], eax
   	mov    eax, dword[ebp - 8]
   	mov    dword[esp + 12], eax
   	mov    dword[esp + 8], 0
   	mov    eax, dword[ebp - 24]
   	mov    dword[esp + 4], eax
   	mov    eax, dword[ebp - 8]
   	mov    dword[esp], eax
   	call   create_list
   	mov    dword[ebx],eax						; l->next = create_list(l, n, 0, l, pr)
   	mov    eax, dword[ebp - 8]
   	mov    dword[eax + 4], 0					; l->prev = NULL
   	mov    eax,dword[ebp - 8]
   	mov    dword[eax + 8], -1					; l->value = -1
   	mov    dword[ebp - 32], 0					
.main_for:										; for (int i = 0; i < m; i++)
	mov    eax, dword[ebp - 32]
   	cmp    eax, dword[ebp - 28]
   	jge    .end_of_for
   	lea    eax, [ebp - 40]						; [ebp - 40] -- int b
   	mov    dword[esp + 12], eax
   	lea    eax,[ebp - 36]						; [ebp - 36] -- int a
   	mov    dword[esp + 8], eax
   	mov    dword[esp + 4], sc_str
   	mov    eax, dword[ebp - 16]
   	mov    dword[esp], eax
   	call   fscanf								; fscanf(fr, "%d%d", &a, &b)
   	mov    eax, dword[ebp - 12]
   	mov    dword[esp + 12], eax
   	mov    eax, dword[ebp - 40]
   	dec    eax
   	mov    dword[esp + 8], eax
   	mov    eax, dword[ebp - 36]
   	dec    eax
   	mov    dword[esp + 4], eax
   	mov    eax, dword[ebp - 8]
   	mov    dword[esp], eax
   	call   transform_list						; l = transform_list(l, a - 1, b - 1, pr)
   	mov    dword[ebp - 8], eax
   	lea    eax,[ebp - 32]
   	inc    dword[eax]
   	jmp    .main_for
.end_of_for:
	mov    dword[esp + 4], out_fmt
   	mov    dword[esp], out_str
   	call   fopen
   	mov    dword[ebp - 20], eax					; fp = fopen("output.txt", "wt");
   	mov    eax, dword[ebp - 20]
   	mov    dword[esp + 4], eax
   	mov    eax, dword[ebp - 8]
   	mov    dword[esp], eax
   	call   print_list							; print_list(l, fp)
   	mov    eax, dword[ebp - 12]
   	mov    dword[esp + 4], eax
   	mov    eax, dword[ebp - 8]
   	mov    dword[esp], eax
   	call   delete_list
   	mov    dword[ebp - 8], eax					; l = delete_list(l, pr)
   	mov    eax, dword[ebp - 20]
   	mov    dword[esp], eax
   	call   fclose								; fclose(fp)
   	mov    eax, dword[ebp - 16]
   	mov    dword[esp], eax
   	call   fclose								; fclose(fr)
   	xor    eax, eax
   	mov    ebx, dword[ebp - 4]
   	leave  
   	ret    
	
create_list:   									; list* create_list(list *l, int n, int i, node *p, node **pr)
	push   ebp
   	mov    ebp, esp
   	push   ebx
   	sub    esp, 36
   	cmp    dword[ebp + 12], 0					; if (!n)
   	jne    .not_n
   	mov    dword[ebp - 8], 0					; return NULL
   	jmp    .end_of_create_list
.not_n:	
	mov    dword[esp], 12						
   	call   malloc
   	mov    dword[ebp + 8], eax					; l = (list *)malloc(sizeof(list))
   	mov    edx, dword[ebp + 8]
   	mov    eax, dword[ebp + 16]
   	mov    dword[edx + 8], eax					; l->value = i
   	mov    edx, dword[ebp + 8]
   	mov    eax, dword[ebp + 20]
   	mov    dword[edx + 4], eax					; l->prev = p
   	mov    ebx, dword[ebp + 8]
   	mov    eax, dword[ebp + 24]
   	mov    dword[esp + 16], eax
   	mov    eax, dword[ebp + 8]
   	mov    dword[esp + 12], eax
   	mov    eax, dword[ebp + 16]
   	inc    eax
   	mov    dword[esp + 8], eax
   	mov    eax, dword[ebp + 12]
   	dec    eax
   	mov    dword[esp + 4], eax
   	mov    eax, dword[ebp + 8]
   	mov    eax, dword[eax]
   	mov    dword[esp], eax
   	call   create_list
   	mov    dword[ebx], eax						; l->next = create_list(l->next, n - 1, i + 1, l, pr)
   	mov    eax, dword[ebp + 16]
   	lea    ecx, [eax * 4]
   	mov    edx, dword[ebp + 24]
   	mov    eax, dword[ebp + 8]
   	mov    dword[edx + ecx], eax				; pr[i] = l
   	mov    eax, dword[ebp + 8]
   	mov    dword[ebp - 8], eax					; return l
.end_of_create_list:
	mov    eax, dword[ebp - 8]
   	add    esp, 36
   	pop    ebx
   	pop    ebp
   	ret
    
transform_list:									; list * transform_list(list *l, int a, int b, node **pr)
   	push   ebp
   	mov    ebp, esp
   	push   ebx
   	sub    esp, 4
   	mov    ebx, dword[ebp + 8]					
   	mov    eax, dword[ebp + 12]
   	lea    ecx, [eax * 4]
   	mov    edx, dword[ebp + 20]
   	mov    eax, dword[ebx]
   	cmp    eax, dword[edx + ecx]
   	jne    .do_nothing							; if (l->next == pr[a])
   	mov    eax, dword[ebp + 8]
   	mov    dword[ebp - 8], eax
   	jmp    .end_of_transform_list				; return l
.do_nothing:	
	mov    eax, dword[ebp + 12]
   	lea    edx, [eax * 4]
   	mov    eax, dword[ebp + 20]
   	mov    eax, dword[edx + eax]
   	mov    ecx, dword[eax + 4]
   	mov    eax, dword[ebp + 16]
   	lea    edx, [eax * 4]
   	mov    eax, dword[ebp + 20]
   	mov    eax, dword[edx + eax]
   	mov    eax, dword[eax]
   	mov    dword[ecx], eax						; pr[a]->prev->next = pr[b]->next
   	mov    eax, dword[ebp + 16]
   	lea    edx, [eax * 4]
   	mov    eax, dword[ebp + 20]
   	mov    eax, dword[eax + edx]
   	cmp    dword[eax], 0
   	je     .last_not_exist									; if (pr[b]->next)
   	mov    eax, dword[ebp + 16]
   	lea    edx, [eax * 4]
   	mov    eax, dword[ebp + 20]
   	mov    eax, dword[eax + edx]
   	mov    ecx, dword[eax]
   	mov    eax, dword[ebp + 12]
   	lea    edx, [eax * 4]
   	mov    eax, dword[ebp + 20]
   	mov    eax, dword[eax + edx]
   	mov    eax, dword[eax + 4]
   	mov    dword[ecx + 4], eax								; pr[b]->next->prev = pr[a]->prev
.last_not_exist:	
	mov    eax, dword[ebp + 8]
   	mov    ecx, dword[eax]
   	mov    eax, dword[ebp + 16]
   	lea    edx, [eax * 4]
   	mov    eax, dword[ebp + 20]
   	mov    eax, dword[eax + edx]
   	mov    dword[ecx + 4], eax								; l->next->prev = pr[b]
   	mov    eax, dword[ebp + 16]
   	lea    edx, [eax * 4]
   	mov    eax, dword[ebp + 20]
   	mov    edx, dword[eax + edx]
   	mov    eax, dword[ebp + 8]
   	mov    eax, dword[eax]
   	mov    dword[edx], eax									; pr[b]->next = l->next
   	mov    ecx, dword[ebp + 8]
   	mov    eax, dword[ebp + 12]
   	lea    edx, [eax * 4]
   	mov    eax, dword[ebp + 20]
   	mov    eax, dword[eax + edx]
   	mov    dword[ecx], eax									; l->next = pr[a]
   	mov    eax, dword[ebp + 12]
   	lea    edx, [eax * 4]
   	mov    eax, dword[ebp + 20]
   	mov    edx, dword[eax + edx]
   	mov    eax, dword[ebp + 8]
   	mov    dword[edx + 4], eax								; pr[a]->prev = l
   	mov    eax, dword[ebp + 8]
   	mov    dword[ebp - 8], eax
.end_of_transform_list:	
	mov    eax, dword[ebp - 8]								; return l
   	add    esp, 4
   	pop    ebx
   	pop    ebp
   	ret    
	
print_list:   												; void print_list(list *l, FILE *fp)
	push   ebp
   	mov    ebp, esp
   	sub    esp, 24	
   	mov    eax, dword[ebp + 8]
   	mov    eax , dword[eax]
   	mov    dword[ebp + 8], eax								; l = l->next
.print_while:	
	cmp    dword[ebp + 8], 0								; while (l != NULL)
   	je     .end_of_print_list
   	mov    eax, dword[ebp + 8]
   	mov    eax, dword[eax + 8]
   	inc    eax
   	mov    dword[esp + 8], eax
   	mov    dword[esp + 4], pr_str
   	mov    eax, dword[ebp + 12]
   	mov    dword[esp], eax
   	call   fprintf											; fprintf(fp, "%d ", l->value + 1)
   	mov    eax, dword[ebp + 8]
   	mov    eax, dword[eax]
   	mov    dword[ebp + 8], eax								; l = l->next
   	jmp    .print_while
.end_of_print_list:	
	leave  
   	ret    

delete_list:												; list* delete_list(list *l, node **pr)   	
	push   ebp
   	mov    ebp, esp
   	sub    esp, 24
   	cmp    dword[ebp + 8], 0								; if (l == NULL)
   	jne    .not_null								
   	mov    eax, dword[ebp + 12]
   	mov    dword[esp], eax
   	call   free												; free(pr)
   	mov    eax, dword[ebp + 8]
   	mov    dword[ebp - 4], eax								; return l
   	jmp    .end_of_delete_list
.not_null:	
	mov    eax, dword[ebp + 12]
   	mov    dword[esp + 4], eax
   	mov    eax, dword[ebp + 8]
   	mov    eax, dword[eax]
   	mov    dword[esp], eax
   	call   delete_list										; delete_list(l->next, pr)
   	mov    eax, dword[ebp + 8]
   	mov    dword[esp], eax
   	call   free												; free(l)
   	mov    dword[ebp - 4], 0								; return NULL
.end_of_delete_list:	
	mov    eax,dword[ebp - 4]
   	leave  
   	ret 