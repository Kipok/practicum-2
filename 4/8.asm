%include "io.inc"

CEXTERN fopen
CEXTERN fclose
CEXTERN qsort
CEXTERN fscanf
CEXTERN fprintf

section .rodata
    sc_str db '%d', 0
    pr_str db '%d ', 0
    inopen db 'input.txt', 0
    inform db 'rt', 0
    outopen db 'output.txt', 0
    outform db 'wt', 0

section .bss
    a resd 10001

section .text
    global CMAIN
CMAIN: 
    push ebp
    push edi
    push ebx
    mov ebp, esp
    and esp, -16
    push dword[ebp + 4]
    push ebp
    mov ebp, esp
    sub esp, 40
    mov dword[esp], inopen
    mov dword[esp + 4], inform
    call fopen
    mov dword[ebp - 4], eax           ; fr = fopen("input.txt", "rt")

    mov dword[esp], outopen
    mov dword[esp + 4], outform
    call fopen
    mov dword[ebp - 8], eax           ; fr = fopen("output.txt", "wt")
    mov ebx, 0                        ; i = 0
.while:
    mov eax, dword[ebp - 4]
    mov dword[esp], eax
    mov dword[esp + 4], sc_str
    lea eax, [a + ebx * 4]
    mov dword[esp + 8], eax
    call fscanf
    inc ebx
    cmp eax, 1
    jz .while
    
    dec ebx
    mov dword[esp], a
    mov dword[esp + 4], ebx
    mov dword[esp + 8], 4
    mov dword[esp + 12], compare
    call qsort
    mov edi, 0                         ; j = 0
    cmp edi, ebx
    jge .end
.for:
    mov eax, dword[ebp - 8]
    mov dword[esp], eax
    mov dword[esp + 4], pr_str
    lea eax, [a + edi * 4]
    mov eax, dword[eax]
    mov dword[esp + 8], eax
    call fprintf
    inc edi
    cmp edi, ebx
    jl .for
.end:
    mov eax, dword[ebp - 4]
    mov dword[esp], eax
    call fclose
    mov eax, dword[ebp - 8]
    mov dword[esp], eax
    call fclose 
    
    add esp, 40
    pop ebp
    mov esp, ebp
    pop edi
    pop ebp
    pop ebx
    xor eax, eax
    ret


compare:
    push ebp
    mov ebp, esp
    mov eax, dword[ebp + 8]
    mov eax, dword[eax]
    mov ecx, dword[ebp + 12]
    mov ecx, dword[ecx]
    cmp eax, 0
    je .end_check
    cmp ecx, 0
    je .end_check
    cmp eax, 0

    jl .sec_check
    cmp ecx, 0
    jg .end_check
    mov eax, 1
    jmp .end
.sec_check:
    cmp ecx, 0
    jl .end_check
    mov eax, -1
    jmp .end
.end_check:
    sub eax, ecx
.end:
    mov esp, ebp
    pop ebp
    ret
